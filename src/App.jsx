import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { Landing } from "./pages/Landing";
import { HomeLayout } from "./pages/HomeLayout";
import { About } from "./pages/About";
import { Error } from "./pages/Error";
import { Cocktail } from "./pages/Cocktail";
import { Newsletter } from "./pages/Newsletter";
import { SinglePageError } from "./pages/SinglePageError";

import { loader as landingLoader } from "./pages/Landing";
import { loader as singleCocktailLoader } from "./pages/Cocktail";
import { action as newsletterAction } from "./pages/Newsletter";
const router = createBrowserRouter([
  {
    path: "/",
    element: <HomeLayout />,
    errorElement: <Error />,
    children: [
      {
        index: true,
        element: <Landing />,
        errorElement: <SinglePageError />,
        loader: landingLoader,
      },
      {
        path: "cocktail/:id",
        errorElement: <SinglePageError />,
        loader: singleCocktailLoader,
        element: <Cocktail />,
      },
      {
        path: "newsletter",
        element: <Newsletter />,
        action: newsletterAction,
        errorElement: <SinglePageError />,
      },
      {
        path: "about",
        element: <About />,
      },
    ],
  },
]);

const App = () => {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
};

export default App;
