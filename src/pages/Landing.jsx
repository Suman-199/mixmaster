import axios from "axios";
import { useLoaderData } from "react-router-dom";
import { SearchForm } from "../components/SearchForm";
import { CocktailList } from "../components/CocktailList";
// import CocktailList from "../components/CocktailList";
const cocktailSearchUrl =
  "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=";

export const loader = async ({ request }) => {
  const url = new URL(request.url);
  const searchTerm = url.searchParams.get("search") || "";
  const response = await axios.get(`${cocktailSearchUrl}${searchTerm}`);
  // console.log(response);
  // return response.data.drinks;
  return { drinks: response.data.drinks, searchTerm };
};

export const Landing = () => {
  const { drinks, searchTerm } = useLoaderData();
  // console.log(drinks);
  return (
    <>
      <SearchForm searchTerm={searchTerm} />
      <CocktailList drinks={drinks} />
    </>
  );
};
