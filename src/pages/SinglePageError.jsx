import { useRouteError } from "react-router-dom";

export const SinglePageError = () => {
  const error = useRouteError();
  console.log(error.message);
  return (
    <>
      <h2>There was an error...</h2>
    </>
  );
};
